#-------------------------------------------------
#
# Project created by QtCreator 2019-08-28T00:59:42
#
#-------------------------------------------------

QT       -= core gui

TARGET = qrencode
TEMPLATE = lib
CONFIG += lib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS HAVE_CONFIG_H



# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
#-------------- QR LIB 纯C源码 -------------------
    $$PWD/bitstream.c \
    $$PWD/mask.c \
    $$PWD/mmask.c \
    $$PWD/mqrspec.c \
    $$PWD/qrencode.c \
    $$PWD/qrinput.c \
    $$PWD/qrspec.c \
    $$PWD/rsecc.c \
    $$PWD/split.c

HEADERS += \
    $$PWD/bitstream.h \
    $$PWD/mask.h \
    $$PWD/mmask.h \
    $$PWD/mqrspec.h \
    $$PWD/qrenc.h \
    $$PWD/qrencode.h \
    $$PWD/qrinput.h \
    $$PWD/qrspec.h \
    $$PWD/rsecc.h \
    $$PWD/split.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
